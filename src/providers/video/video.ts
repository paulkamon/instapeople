import { Http,Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { GlobalUrlProvider } from '../global-url/global-url';
import { HTTP } from '@ionic-native/http';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media';

/*
  Generated class for the VideoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class VideoProvider {
  apiUrl = "";
  api = "";

  constructor(public streamingMedia: StreamingMedia, public httpn: HTTP,public http: Http,public globalUrlPrvd:GlobalUrlProvider) {
    console.log('Hello VideoProvider Provider');
	this.api = this.globalUrlPrvd.apiUrl;
	this.apiUrl = this.globalUrlPrvd.apiUrl;
  }

  playVideo(path){
    let options: StreamingVideoOptions = {
      successCallback: () => { console.log('Video played') },
      errorCallback: (e) => { console.log('Error streaming') },
      shouldAutoClose: true,
      controls: true
    };
    
    this.streamingMedia.playVideo(path, options);
  }

  getFree() {
		return new Promise<any>((resolve,reject) => {
			this.httpn.get(this.apiUrl+'v2/videosgratuites', {},{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
  }

  getArtistVideo(artistId,clientId) {
		return new Promise<any>((resolve,reject) => {
			this.httpn.get(this.apiUrl+'v2/videosdunartiste/'+artistId+"/"+clientId, {},{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
  }

  getAll() {
		return new Promise<any>((resolve,reject) => {
			this.httpn.get(this.apiUrl+'v2/videos', {},{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
  }

}
