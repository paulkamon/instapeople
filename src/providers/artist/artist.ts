import { Http,Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { GlobalUrlProvider } from "../global-url/global-url";
import 'rxjs/add/operator/map';
import { HTTP } from '@ionic-native/http';

/*
  Generated class for the GenderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ArtistProvider {
  apiUrl:string = "";
  data: any;
  dataAlbumPaginate: any;
  dataMusicPaginate: any;
  dataVideo: any;
  dataMusic: any;
  dataAlbum: any;

  constructor(public httpn: HTTP,public http: Http,public globalUrlPrvd: GlobalUrlProvider) {
    console.log('Hello ArtistProvider Provider');
    this.apiUrl = this.globalUrlPrvd.apiUrl;
  }

  getAllArtist() {
		return new Promise<any>((resolve,reject) => {
			this.httpn.get(this.apiUrl+'v2/listedesartistesavecphotos', {},{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
  }

  getArtisteRecommandes() {
		return new Promise<any>((resolve,reject) => {
			this.httpn.get(this.apiUrl+'v2/artistesrecommandes', {},{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
  }

  getClientArtists(clientId) {
		return new Promise<any>((resolve,reject) => {
			this.httpn.get(this.apiUrl+'v2/artistesdunclientabonne/'+clientId, {},{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
  }

  get(token){
    return new Promise<any>(resolve => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append("Authorization","Bearer "+token);
      this.http.get(this.apiUrl+'artist/recommended', {headers:headers})
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  getAlbum(token){
    return new Promise<any>(resolve => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append("Authorization","Bearer "+token);
      this.http.get(this.apiUrl+'album/recommended', {headers:headers})
        .map(res => res.json())
        .subscribe(data => {
          this.dataAlbum = data;
          resolve(this.dataAlbum);
        });
    });
  }

  getMusic(token){
    return new Promise<any>(resolve => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append("Authorization","Bearer "+token);
      this.http.get(this.apiUrl+'music/recommended', {headers:headers})
        .map(res => res.json())
        .subscribe(data => {
          this.dataMusic = data;
          resolve(this.dataMusic);
        });
    });
  }

  getVideoRecommanded(token){
    return new Promise<any>(resolve => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append("Authorization","Bearer "+token);
      this.http.get(this.apiUrl+'video/recommended', {headers:headers})
        .map(res => res.json())
        .subscribe(data => {
          this.dataVideo = data;
          resolve(this.dataVideo);
        });
    });
  }

  getVideo(token,number){
    return new Promise<any>(resolve => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append("Authorization","Bearer "+token);
      this.http.get(this.apiUrl+'videos?$page='+number, {headers:headers})
        .map(res => res.json())
        .subscribe(data => {
          this.dataVideo = data;
          resolve(this.dataVideo);
        });
    });
  }

  getMusicPaginate(token,number){
    return new Promise<any>(resolve => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append("Authorization","Bearer "+token);
      this.http.get(this.apiUrl+'musics?page='+number, {headers:headers})
        .map(res => res.json())
        .subscribe(data => {
          this.dataMusicPaginate = data;
          resolve(this.dataMusicPaginate);
        });
    });
  }
}
