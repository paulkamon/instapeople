import { Http,Headers } from '@angular/http';
import { HTTP } from '@ionic-native/http';
import { Injectable } from '@angular/core';
import { GlobalUrlProvider } from "../global-url/global-url";

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {
  apiUrl:string = "";


  constructor(public httpn: HTTP,public http: Http,public globalUrlPrvd:GlobalUrlProvider) {
    this.apiUrl = this.globalUrlPrvd.apiUrl;
    console.log('Hello AuthProvider Provider');
    this.apiUrl = this.globalUrlPrvd.apiUrl;
  }

  login(credentials,uri) {
		return new Promise<any>((resolve,reject) => {
			this.httpn.post(this.apiUrl+uri, credentials,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
  }

  resetPwd(dataEmail) {
		return new Promise<any>((resolve,reject) => {
			this.httpn.post(this.apiUrl+'v2/passwordforget', dataEmail,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
  }
  
  sign(credentials,uri){
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      this.http.post(this.apiUrl+uri, JSON.stringify(credentials), {headers:headers})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
    });
  }

  userInformation(token){
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append("Authorization","Bearer "+token);
      this.http.post(this.apiUrl+'informations', {}, {headers:headers})
        .subscribe(res => {
          resolve(res.json());
        }, (err) => {
          reject(err);
        });
    });
   
  }

  signInWithFacebook(data){

  }

  signUp(data){

  }

  update(data,id) {
		return new Promise<any>((resolve,reject) => {
			this.httpn.post(this.apiUrl+'v2/modifierprofiluser/'+id, data,{}).then((data)=>{
				let resp = JSON.parse(data.data);
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
  }

  payer(data) {
		return new Promise<any>((resolve,reject) => {
			this.httpn.get('http://webivoiremedia.net/paiement/index.php', data,{}).then((data)=>{
        // let resp = JSON.parse(data.data);
        let resp = data.data;
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
  }
  // payer(data) {
	// 	return new Promise<any>((resolve,reject) => {
	// 		this.httpn.post(this.apiUrl+'v2/payerportailhum', data,{}).then((data)=>{
  //       // let resp = JSON.parse(data.data);
  //       let resp = data.data;
	// 			console.error(resp);
	// 			resolve(resp);
	// 		}).catch((error)=>{
	// 			reject(error);
	// 		})
	// 	}).then((uu)=> uu)
	// 	.catch(err => Promise.reject(err || 'err'));
  // }

  getUrln(url) {
  
		return new Promise<any>((resolve,reject) => {
			this.httpn.get(url, {},{}).then((data)=>{
        // let resp = JSON.parse(data.data);
        let resp = data.data;
				console.error(resp);
				resolve(resp);
			}).catch((error)=>{
				reject(error);
			})
		}).then((uu)=> uu)
		.catch(err => Promise.reject(err || 'err'));
  }

  getUrl(url){
    return new Promise((resolve, reject) => {
      // let headers = new Headers();
      // headers.append('Content-Type', 'application/json');
      // headers.append("Authorization","Bearer "+token);
      this.http.get(url, {})
        .subscribe(res => {
          resolve(res.text());
        }, (err) => {
          reject(err);
        });
    });
  }

}
