import { Injectable } from '@angular/core';
import { ToastController,AlertController,LoadingController  } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
/*
  Generated class for the UiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UiProvider {
  isConnect: any;
  loader:any;

  constructor(public toastCtrl: ToastController,
              public alertCtrl: AlertController,
              public loadingCtrl: LoadingController,
              public network:Network,
              public storage: Storage
    ){
    console.log('Hello UiProvider Provider');
  }

  presentToast(message) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  showAlert(title,message) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    return alert;
  }

  presentLoading(message) {
    const loader = this.loadingCtrl.create({
      content: message,
    });
    return loader;
  }

  networkState(){
    return this.network.onDisconnect();
  }

  setStorage(item,credentials){
    return this.storage.set(item, credentials);
  }

  getStorage(item){
    return this.storage.get(item);
  }

  removeStorage(item){
    this.storage.remove(item);
  }
}
