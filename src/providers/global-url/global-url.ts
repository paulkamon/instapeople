import { Injectable } from '@angular/core';

/*
  Generated class for the GlobalUrlProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalUrlProvider {

  // apiUrl:string = "http://178.33.227.9:44399/STAR_LIFE/api/";
  apiUrl:string = "http://webivoiremedia.net/site/api/";
  //apiUrl:string = "http://192.168.1.19/digimusicapp/backend/api/";
  //apiStorage:string = "http://192.168.1.19/digimusicapp/backend/";

  constructor() { 
    console.log('Hello GlobalUrlProvider Provider');
  }

}
