import { Http,Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { GlobalUrlProvider } from "../global-url/global-url";
import 'rxjs/add/operator/map';
/*
  Generated class for the GenderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GenderProvider {
  apiUrl = "";
  data: any;
  dataPaginate: any;

  constructor(public http: Http,public globalUrlPrvd:GlobalUrlProvider) {
    console.log('Hello GenderProvider Provider');
    this.apiUrl = this.globalUrlPrvd.apiUrl;
  }

  get(token){
    if (this.dataPaginate) {
      return Promise.resolve(this.dataPaginate);
    }
    
    return new Promise(resolve => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append("Authorization","Bearer "+token);
      this.http.get(this.apiUrl+'gender/top', {headers:headers})
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  getPaginate(token,nombre){
    if (this.dataPaginate) {
      return Promise.resolve(this.dataPaginate);
    }
    return new Promise(resolve => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append("Authorization","Bearer "+token);
      this.http.get(this.apiUrl+'genders?page='+nombre, {headers:headers})
        .map(res => res.json())
        .subscribe(data => {
          this.dataPaginate = data;
          resolve(this.dataPaginate);
        });
    });
  }
}
