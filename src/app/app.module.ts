import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { File } from '@ionic-native/file';
import { Network } from '@ionic-native/network';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
import { HTTP } from '@ionic-native/http';

import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Camera } from '@ionic-native/camera';
import { MediaCapture} from '@ionic-native/media-capture';

import { StreamingMedia } from '@ionic-native/streaming-media';


import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


import { UtilsProvider } from '../providers/utils/utils';
import { ConnectivityProvider } from '../providers/connectivity/connectivity';
import { AuthProvider } from "../providers/auth/auth";
import { UiProvider } from "../providers/ui/ui";
import { GenderProvider } from '../providers/gender/gender';
import { ArtistProvider } from '../providers/artist/artist';
import { VideoProvider } from '../providers/video/video';
import { GlobalUrlProvider } from '../providers/global-url/global-url';

import { OneSignal } from '@ionic-native/onesignal';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    HttpClientJsonpModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp, {
      backButtonText: '',
    }
  )],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    File,
    Network,
    HTTP,
    Camera,
    File,
    OneSignal,
    VideoProvider,
    MediaCapture,
    UtilsProvider,
    ConnectivityProvider,
    StreamingMedia,
    Network,
    AuthProvider,
    UiProvider,
    GenderProvider,
    ArtistProvider,
    GlobalUrlProvider,
    InAppBrowser, 
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
