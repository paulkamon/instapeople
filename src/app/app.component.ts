import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, ModalController, AlertController, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { AboutPage } from '../pages/about/about';
import { UtilsProvider } from '../providers/utils/utils';
import { ConnectivityProvider } from '../providers/connectivity/connectivity';
import { Storage } from '@ionic/storage';

import { OneSignal } from '@ionic-native/onesignal';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = HomePage;
  pseudo:string;
  email:string;
  photoUrl:string;
  user;

  constructor(public app: App, platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,public events: Events,
    private connectivityService: ConnectivityProvider,
    public storage: Storage,
    private utils: UtilsProvider,
    public modalCtrl: ModalController,public alertCtrl: AlertController,private oneSignal: OneSignal) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      this.oneSignal.startInit('e6ec290a-9777-4771-bdff-13838b3e4bee', '247963065442');

      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
  
    //   // for debugging purposes
    // this.oneSignal.setLogLevel({ logLevel: 4, visualLevel: 4 });

      this.oneSignal.handleNotificationReceived().subscribe((data: any) => {
      // do something when notification is received
      console.log('push notification received, data: ', data);
      });
  
      this.oneSignal.handleNotificationOpened().subscribe((data: any) => {
        // do something when a notification is opened
        console.log('push notification opened, data: ', data);
        // this.nav.setRoot('MesAbonnementsPage');
      });
      // window['plugins'].OneSignal.setExternalUserId('my_unique_name');
      this.oneSignal.endInit();


      this.getUser();
    });
    platform.registerBackButtonAction(() => { 
        if (this.nav.canGoBack()){
        this.nav.pop();
        }else{console.log('cannot go back');
        }
 }, 600);

    events.subscribe('user:created', (user, time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      if (user) {
        this.user=user;
        console.log('Welcome', user.name, 'at', time);
        this.pseudo = user.name+" "+user.surname;
        this.email = user.email;
        window['plugins'].OneSignal.setExternalUserId(user.id.toString());
        if(user.lien_avatar){   
          this.photoUrl = user.lien_avatar;
        }else{
          this.photoUrl = 'assets/imgs/profile.png';
        }
      } else {
        this.pseudo = "";
        this.email = "";
      }
      // this.viewCtrl._willEnter();
    });
  }

  getUser(){
    this.storage.get('user').then((user)=>{
      console.log(user);
      this.user=user;
      if (user) {
        window['plugins'].OneSignal.setExternalUserId(user.id.toString());
        this.pseudo = user.name+" "+user.surname;
        this.email = user.email;
        if(user.lien_avatar){   
          this.photoUrl = user.lien_avatar;
        }else{
          this.photoUrl = 'assets/imgs/profile.png';
        }
      } else {
        this.pseudo = "";
        this.email = "";
      }
   }).catch(()=>{
  });
  }


  home() {
    // this.viewCtrl._willEnter();
    this.nav.setRoot(HomePage);
    }
    
  login(){
    this.nav.setRoot('Login2Page',{});
  }

  register(){
    this.nav.setRoot('RegisterPage');
  }

  edit(){
    this.nav.setRoot('EditProfilPage',{});
  }

  picture(){
    this.nav.setRoot('PicturePage',{});
  }

  abonnements(){
    this.nav.setRoot('MesAbonnementsPage')
  }

  artistes(){
    this.nav.setRoot('ArtistesPage')
  }

  presentModal() {
    const modal = this.modalCtrl.create('PicturePage');
    modal.onDidDismiss(data => {
      console.log(data);
      // this.viewCtrl._willEnter();
    });
    modal.present();
  }

  logout(){
    // this.navCtrl.setRoot(LoginPage,{});  
  this.storage.clear().then(()=>{
    this.user=null;
          this.app.getRootNav().setRoot(HomePage);
          // window.location.reload();
        console.log('cleared');
        window['plugins'].OneSignal.removeExternalUserId();
        // this.viewCtrl._willEnter();
      })
      .catch(()=>{
        this.utils.makeToast('Veuillez réessayer svp!').present();
      });

  }

  showConfirm() {
    const confirm = this.alertCtrl.create({
      // title: 'Use this lightsaber?',
      message: 'voulez vous vraiment vous déconnecter?',
      buttons: [
        {
          text: 'annuler',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'confirmer',
          handler: () => {
            console.log('Agree clicked');
            this.logout();
          }
        }
      ]
    });
    confirm.present();
  }
}
