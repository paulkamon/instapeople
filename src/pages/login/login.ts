import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Nav, Events } from 'ionic-angular';

import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';
import { AuthProvider } from "../../providers/auth/auth";
import { UiProvider } from "../../providers/ui/ui";
import { AboutPage } from '../about/about';
import { HomePage } from '../home/home';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user;
  dataUser = { email:'',password:'' };

  constructor(public events: Events,private connectivityService: ConnectivityProvider,public navCtrl: NavController, public navParams: NavParams,
    private authPrvd: AuthProvider,
    private uiPrvd: UiProvider,private utils: UtilsProvider,
    public storage: Storage,
    private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  ionViewWillEnter(){
    this.storage.get('user').then((user)=>{
      console.log(user);
      this.user=user;
      if(user){   
        // this.navCtrl.pop();
        this.navCtrl.setRoot(AboutPage)
            this.events.publish('user:created', user, Date.now());
      }
        })
  }

  register(){
    this.navCtrl.push('RegisterPage');
  }

  
login() {
  if(this.connectivityService.isOnline()){
    if (this.dataUser.email && this.dataUser.password) {
    let loader = this.utils.makeLoader('Connexion en cours...');
    loader.present().then(()=>{
      this.authPrvd.login(this.dataUser, 'login').then(response =>{
          loader.dismiss();
          console.log(response);
          this.storage.set('token', response.token);
          this.storage.set('password', this.dataUser.password);
          this.storage.set('user', response.user).then(()=> {
            this.navCtrl.setRoot(HomePage)
            window['plugins'].OneSignal.setExternalUserId(response.user.id.toString());
            this.events.publish('user:created', response.user, Date.now());
          });
          this.utils.makeToast(response.user.name+' est connecté').present();
          
          
        
      }).catch((error)=>{
        console.log(error);

        loader.dismiss();
        this.utils.makeToast('Votre numéro ou votre mot passe est incorrecte!').present();
      })
    })
  } else {
    this.utils.makeToast('Veuillez remplir tous les champs').present();
  }
  }
  else{
    this.utils.makeToast('Veuillez vous connecter à internet').present();
  }
}

  signIn(){
    // this.uiPrvd.presentLoading('Authentification...');
    this.authPrvd.sign(this.dataUser,'login').then(result => {
      // this.uiPrvd.loader.dismiss();
      console.log('result' + result);
      this.uiPrvd.presentToast("Erreur d'authentification !");
    //  this.uiPrvd.setStorage('token',result).then((stock)=>{
    //     console.log(stock);
    //     this.uiPrvd.getStorage('token').then((data)=> {
    //       this.albumPrvd.getMusicLike(data.access_token).then((res) => {
    //           if (res.data.length > 0) {
    //             res.data.forEach(element => {
    //               console.error(element.media);
    //             });
    //           }
    //           this.albumPrvd.getAlbumLike(data.access_token).then((resAlbum)=>{
    //             console.log(resAlbum);
    //             if (resAlbum.data.length > 0) {
    //               resAlbum.data.array.forEach(dat => {
    //                 console.log(dat);
    //               });
    //             }
    //           });
    //       });
    //       this.navCtrl.setRoot(MenuPage);
    //     });
    //   });
}, (err) => {
  console.log("error");
      // this.uiPrvd.loader.dismiss();
// this.uiPrvd.presentToast("Erreur d'authentification !");
    });
  }

  presentPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Reinitialiser mot de passe?',
      // message: 'Are you sure ?',
      inputs: [
        {
          name: 'email',
          placeholder: 'Entrez votre email'
        }
      ],
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'confirmer',
          handler: data => {
            if (data.email) {
             this.resetPwd(data.email)
            } else {
              // invalid login
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }

  resetPwd(email) {
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('');
      loader.present().then(()=>{
        let datas = {
          email:email,
        }
        this.authPrvd.resetPwd(datas).then(response =>{
            loader.dismiss();
            // this.response = response;
            console.log(response);
            this.showAlert();
        
        }).catch((error)=>{
          console.log(error);
  
          loader.dismiss();
          this.utils.makeToast('une erreur est survenue').present();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }

  showAlert() {
    const alert = this.alertCtrl.create({
      title: 'Mot de passe réinitialisé avec succès',
      subTitle: 'un mail contenant votre nouveau mot de passe a été envoyé',
      buttons: ['OK']
    });
    alert.present();
  }

}
