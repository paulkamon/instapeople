import { Component } from '@angular/core';
import { NavController,ModalController,AlertController} from 'ionic-angular';
import { MoviePage } from '../movie/movie';
import { ArtistProvider } from "../../providers/artist/artist";
import { VideoProvider } from "../../providers/video/video";
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  loaded:boolean = true;
  hasSubscription:boolean;
  listSubscription:any[] = [{
    id: '',    
    datedebut: '',
    datefin:'',
    artisteforfait: {
      prix: '',    
      artiste: {
        id: '',    
        nom: '',
        photo: {
          id: '',    
          path: ''
        },
      }
    }
  }];
  user;
  freeVideos :any[] = [{
    id: '',    
    titre: '',
    artiste_id:'',
    description:'',
    cover: {
      id: '',    
      path: ''
    },
    fichier: {
      id: '',    
      path: ''
    },
  }];

  recmdArtistslimit :any[] = [{
    id: '',    
    nom: '',
    photo: {
      id: '',    
      path: ''
    },
    artistesforfaits: [{
      id: '',    
      prix: '',
      forfait: {
        libelle: ''    
      }
    }]
  }];

  recmdArtists :any[] = [{
    id: '',    
    nom: '',
    photo: {
      id: '',    
      path: ''
    },
    artistesforfaits: [{
      id: '',    
      prix: '',
      forfait: {
        libelle: ''    
      }
    }]
  }];

  allArtistsLimit :any[] = [{
    id: '',    
    nom: '',
    photo: {
      id: '',    
      path: ''
    },
    artistesforfaits: [{
      id: '',    
      prix: '',
      forfait: {
        libelle: ''    
      }
    }]
  }];

  allArtists :any[] = [{
    id: '',    
    nom: '',
    photo: {
      id: '',    
      path: ''
    },
    artistesforfaits: [{
      id: '',    
      prix: '',
      forfait: {
        libelle: ''    
      }
    }]
  }];

  constructor(public artistPrvd: ArtistProvider, public vidPrvd: VideoProvider,
     public navCtrl: NavController,public modalController: ModalController,
     public storage: Storage,
    private alertCtrl: AlertController,
    private connectivityService: ConnectivityProvider,private utils: UtilsProvider) {

  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad RegisterPage');
    this.getFreeVideo();
  }
  ionViewWillEnter(){
    this.storage.get('user').then((user)=>{
      console.log(user);
      this.user=user;
      if (this.user) {
        this.getClientArtists(this.user.id);
        let id = user.id.toString();
          console.log('id ',id);
          window['plugins'].OneSignal.setExternalUserId(id);
      }
    })
  }
  async openMovie() {
    if (!this.user) {
      this.navCtrl.push('Login2Page',{});
    }else{
      const modal = await this.modalController.create('ArtisteDetailPage');
      return await modal.present();
    } 
    
  }

  seeMore(){
    this.navCtrl.push('ArtistesPage',{ type: "more" })
  }

  seeMoreAll(){
    this.navCtrl.push('ArtistesPage',{ type: "all" })
  }

  seeMoreRcmd(){
    this.navCtrl.push('ArtistesPage',{artistsR: this.recmdArtists, type: "recommended" })
  }

  seeMoreVA(){
    this.navCtrl.push('MesAbonnementsPage',{})
  }

  playVideo(i){
    this.vidPrvd.playVideo(this.freeVideos[i].fichier.path)
  }

  async detailRcmdA(i) {
    if (!this.user) {
      this.navCtrl.push('Login2Page',{});
    }else{
      const modal = await this.modalController.create('ArtisteDetailPage',{artist: this.recmdArtists[i]});
      return await modal.present();
    } 
    
  }

  async detailallA(i) {
    if (!this.user) {
      this.navCtrl.push('Login2Page',{});
    }else{
      const modal = await this.modalController.create('ArtisteDetailPage',{artist: this.allArtists[i]});
      return await modal.present();
    } 
  }
  
  async detailVArtist(i) {
    const modal = await this.modalController.create('ArtisteDetailPage',{artist: this.listSubscription[i].artisteforfait.artiste});
    return await modal.present();
}

  getFreeVideo() {
    if(this.connectivityService.isOnline()){
        this.vidPrvd.getFree().then(response =>{
          console.log(response);
          this.freeVideos = response.data;
          this.loaded=true;
          this.getRecommendedArtists();
        }).catch((error)=>{
          this.getRecommendedArtists();
          console.log(error);
          this.loaded=false;
            this.utils.makeToast('erreur de chargement').present();
        })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }

  getFreeVideoR(refresher) {
    if(this.connectivityService.isOnline()){
        this.vidPrvd.getFree().then(response =>{
          refresher.complete()
          console.log(response);
          this.freeVideos = response.data;
          this.loaded=true;
          this.getRecommendedArtists();
        }).catch((error)=>{
          refresher.complete();
          this.loaded=false;
          this.getRecommendedArtists();
          console.log(error);
            // this.utils.makeToast('erreur de chargement').present();
        })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }

  getRecommendedArtists() {
    if(this.connectivityService.isOnline()){
        this.artistPrvd.getArtisteRecommandes().then(response =>{
          console.log(response);
          this.recmdArtists = response.data.reverse();
          // this.recmdArtistslimit = [];
          // for (let index = 0; index < 3; index++) {
          //   this.recmdArtistslimit.push(this.recmdArtists[index]);
          // }
          this.loaded=true;
          this.getAllArtists();
        }).catch((error)=>{
          this.getAllArtists();
          console.log(error);
          this.loaded=false;
            // this.utils.makeToast('error').present();
        })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }

  public imgPath(photo){
    if (photo == "" || photo == null || photo==undefined) {
      return "assets/man.jpg";
    } else {
      return photo.path;
    }
    
  }

  public imgVidPath(photo){
    if (photo == "" || photo == null || photo==undefined) {
      return "assets/instaP.jpeg";
    } else {
      return photo.path;
    }
    
  }

  getAllArtists() {
    if(this.connectivityService.isOnline()){
        this.artistPrvd.getAllArtist().then(response =>{
          console.log(response);
          this.allArtists = response.data;
          // this.allArtistsLimit = [];
          // for (let index = 0; index < 3; index++) {
          //   this.allArtistsLimit.push(this.allArtists[index]);
          // }
          this.loaded=true;
          if (this.user) {
            this.getClientArtists(this.user.id);
          }
        }).catch((error)=>{
          console.log(error);
          this.loaded=false;
            this.utils.makeToast('erreur lors du chargement').present();
            // this.getFreeVideo();
        })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }

  
  getClientArtists(clientId) {
    if(this.connectivityService.isOnline()){
        this.artistPrvd.getClientArtists(clientId).then(response =>{
          this.listSubscription = response.data;
          if (response.data == undefined || response.data == '') {
            this.hasSubscription = false;
          }else{
            this.hasSubscription = true;
          }
          console.log(response);
        }).catch((error)=>{
          this.hasSubscription = false;
          console.log(error);
            this.utils.makeToast('veuillez réessayer').present();
        })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.getFreeVideoR(refresher);
    // refresher.complete()
  }

}
