import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams, Events } from 'ionic-angular';
import { UtilsProvider } from '../../providers/utils/utils';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { Storage } from '@ionic/storage';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { AuthProvider } from "../../providers/auth/auth";
import { AboutPage } from '../about/about';
import { HomePage } from '../home/home';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  picture: string;
  
  user = {
    name:'',
    surname:'',
    typeuser_id:1,
    email:'',
    password:'',
    password_confirmation:'',
    telephone: '',
    detailsphoto:''
  };
  

  constructor(public events: Events,public navCtrl: NavController, public navParams: NavParams, private utils: UtilsProvider,
    public storage: Storage,
    private connectivityService: ConnectivityProvider,
    public camera: Camera,public alertCtrl: AlertController,public authPrvd: AuthProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  actionSheet(){
    this.utils.makeActionSheet('Prendre une photo',[{
      icon: 'aperture',
      text: 'Galerie',
      handler: () => {
        this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
      }
    },{
      icon: 'camera',
      text: 'Camera',
      handler: () => {
        this.takePicture(this.camera.PictureSourceType.CAMERA);
      }
    }]).present();
  }

  takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options : CameraOptions = {
      quality: 50,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      this.picture = 'data:image/jpeg;base64,' + imagePath;
      this.user.detailsphoto = this.picture;
    });
  }


  register(){
    if(this.connectivityService.isOnline()){
      let loader = this.utils.makeLoader('Un instant spv...');

      if (this.user.detailsphoto && this.user.name && this.user.surname && this.user.email && this.user.telephone && this.user.password && this.user.password_confirmation) {   
      loader.present().then(()=>{
        if (this.user.password == this.user.password_confirmation) {
          console.log(JSON.stringify(this.user));
          this.authPrvd.login(this.user,'signup').then(response =>{
              loader.dismiss();
              // console.log(response);
              // this.storage.set('token', response.token);
              // this.storage.set('user', response.user);
              // this.storage.set('password', this.user.password);
              // this.utils.makeToast(response.user.name+' est connecté').present();
              // this.navCtrl.pop();
              this.login();
              // this.navCtrl.setRoot(TabsPage,{});
          }).catch((error)=>{
            console.log(error);
  
            loader.dismiss();
            // this.utils.makeToast(error.error).present();
            let err = JSON.parse(error.error);
            console.log('err', err);
            console.log('errr', err.error);
            this.utils.makeToast(err.error).present();
            
          })
        }
        else{
          loader.dismiss();
          let toast = this.utils.makeToast('Votre mot de passe n\'est pas conforme à celui de la confirmation');
          toast.present();
        }
        
      })
    }else{
      let toast = this.utils.makeToast('Veuillez renseigner tous les champs requis');
      toast.present();
    }
  }
  }

  
login() {
  if(this.connectivityService.isOnline()){
    let loader = this.utils.makeLoader('Connexion en cours...');
    loader.present().then(()=>{
      let data = {
        email: this.user.email,
        password: this.user.password
      };
      this.authPrvd.login(data, 'login').then(response =>{
          loader.dismiss();
          console.log(response);
          this.storage.set('token', response.token);
          this.storage.set('user', response.user);
          this.storage.set('password', this.user.password);
          this.utils.makeToast(response.user.name+' est connecté').present();
          window['plugins'].OneSignal.setExternalUserId(response.user.id.toString());
          this.navCtrl.setRoot(HomePage);
          this.events.publish('user:created', response.user, Date.now());
        
      }).catch((error)=>{
        console.log(error);

        loader.dismiss();
        this.utils.makeToast('Votre numéro ou votre mot passe est incorrecte!').present();
      })
    })
  }
  else{
    this.utils.makeToast('Veuillez vous connecter à internet').present();
  }
}

}
