import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArtistesPage } from './artistes';

@NgModule({
  declarations: [
    ArtistesPage,
  ],
  imports: [
    IonicPageModule.forChild(ArtistesPage),
  ],
})
export class ArtistesPageModule {}
