import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { ArtistProvider } from "../../providers/artist/artist";
import { VideoProvider } from "../../providers/video/video";
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ArtistesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-artistes',
  templateUrl: 'artistes.html',
})
export class ArtistesPage {

  filterData:any[];
  searchTerm: string = '';

  user;
  isloading: boolean = false;
  listArtists :any[] = [{
    id: '',    
    nom: '',
    photo: {
      id: '',    
      path: ''
    }
  }];
  type;
  constructor(public storage: Storage,public artistPrvd: ArtistProvider,private connectivityService: ConnectivityProvider,private utils: UtilsProvider,public modalController: ModalController,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArtistesPage');
    this.type=this.navParams.get('type');
    if (this.type=="all" || this.type =='' || this.type == undefined || !this.type) {
      this.getAllArtists();
    } else {
      this.listArtists=this.navParams.get('artistsR');
      this.filterData=this.listArtists;
    }
  }

  ionViewWillEnter(){
    this.storage.get('user').then((user)=>{
      console.log(user);
      this.user=user;
    })
  }

  push(){
    this.navCtrl.push('PaymentPage');
  }

  public imgPath(photo){
    if (photo == "" || photo == null || photo==undefined) {
      return "assets/man.jpg";
    } else {
      return photo.path;
    }
    
  }
  async detailArtist(i) {
    if (!this.user) {
      this.navCtrl.push('Login2Page',{});
    }else{
      const modal = await this.modalController.create('ArtisteDetailPage',{artist: this.filterData[i]});
      return await modal.present();
    } 
  }

  getAllArtists() {
    this.isloading=true;
    if(this.connectivityService.isOnline()){
        this.artistPrvd.getAllArtist().then(response =>{
          console.log(response);
          this.isloading=false;
          this.listArtists = response.data;
          this.filterData = this.listArtists;
        }).catch((error)=>{
          console.log(error);
          this.isloading=false;
            this.utils.makeToast('error').present();
        })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }


  // ngOnDestroy(){
  //   this.utils.makeToast('destroyed artist').present();
  // }

  onSearch(event){
    console.log(event.target.value);
  }

  getItems(ev: any) {
    console.log('ev '+ev.target.value);

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.filterData = this.listArtists.filter((item) => {
        return (item.nom.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1) /* || (item.profession.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1) */;
      })
    }
  }

  onCancel(event){
    console.log('oncancel '+event.target.value);
    this.filterData = this.listArtists;
  }

}
