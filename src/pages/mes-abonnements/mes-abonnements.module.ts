import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MesAbonnementsPage } from './mes-abonnements';

@NgModule({
  declarations: [
    MesAbonnementsPage,
  ],
  imports: [
    IonicPageModule.forChild(MesAbonnementsPage),
  ],
})
export class MesAbonnementsPageModule {}
