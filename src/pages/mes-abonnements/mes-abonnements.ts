import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { ArtistProvider } from "../../providers/artist/artist";
import { VideoProvider } from "../../providers/video/video";
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the MesAbonnementsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mes-abonnements',
  templateUrl: 'mes-abonnements.html',
})
export class MesAbonnementsPage {

  hasSubscription:boolean;
  hasNoSubscription:boolean;

  listSubscription:any[] = [{
    id: '',    
    datedebut: '',
    datefin:'',
    artisteforfait: {
      prix: '',    
      artiste: {
        id: '',    
        nom: '',
        photo: {
          id: '',    
          path: ''
        },
      }
    }
  }];
  isloading: boolean = false;

  constructor(public modalController: ModalController,public artistPrvd: ArtistProvider, public vidPrvd: VideoProvider,
    public navCtrl: NavController,
    public storage: Storage,
   private connectivityService: ConnectivityProvider,private utils: UtilsProvider) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MesAbonnementsPage');
    this.storage.get('user').then((user)=>{
      console.log(user);
      if (!user) {
        this.navCtrl.pop();
      }else{
        this.getClientArtists(user.id)
      }
    })
  }

  public imgPath(photo){
    if (photo == "" || photo == null || photo==undefined) {
      return "assets/man.jpg";
    } else {
      return photo.path;
    }
    
  }
  
  async detailArtist(i) {
      const modal = await this.modalController.create('ArtisteDetailPage',{artist: this.listSubscription[i].artisteforfait.artiste});
      return await modal.present();
  
  }

  public convertTodate(string){
    return new Date(string.replace(/-/g, '/'));
  }
  
  getClientArtists(clientId) {
    if(this.connectivityService.isOnline()){
      this.isloading=true;
        this.artistPrvd.getClientArtists(clientId).then(response =>{
          this.isloading=false;
          this.listSubscription = response.data;
          if (response.data == null || response.data == '' || response.data == undefined) {
            this.hasNoSubscription = true;
          }else{
            this.hasSubscription = true;
          }
          console.log('data' +response.data);
        }).catch((error)=>{
          this.hasNoSubscription = true;
          console.log(error);
          this.isloading=false;
            this.utils.makeToast('veuillez réessayer').present();
        })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }

}
