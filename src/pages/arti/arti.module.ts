import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArtiPage } from './arti';

@NgModule({
  declarations: [
    ArtiPage,
  ],
  imports: [
    IonicPageModule.forChild(ArtiPage),
  ],
})
export class ArtiPageModule {}
