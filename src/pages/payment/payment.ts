import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
import { AuthProvider } from "../../providers/auth/auth";
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { TabsPage } from '../tabs/tabs';
import { HomePage } from '../home/home';

/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {

  token;

  price;
  indexForfait;
  user;
  artist = {
    id: '',    
    nom: '',
    photo: {
      id: '',    
      path: ''
    },
    artistesforfaits: [{
      id: '',    
      prix: '',
      forfait: {
        libelle: ''    
      }
    }]
  };
  
  data = {
    user_id: '',
    artisteforfait_id: '',
    // montant: '',
    // paymentmeancode:'',
    telephone:'',
  };

  datanew = {
    phone: '',
    channel: '',
    amount: '',
    email:'',
    fname:'',
    lname:'',
    af_id:'',
    user_id:''
  };
  data1 = {
    numero: ''
  };

  constructor(public app: App, public navCtrl: NavController,
     public navParams: NavParams,
  public storage: Storage,
   private connectivityService: ConnectivityProvider,private utils: UtilsProvider,public authPrvd: AuthProvider,private iab: InAppBrowser) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
    this.artist = this.navParams.get('artist');
    this.indexForfait = this.navParams.get('indexForfait');
    this.storage.get('user').then((user)=>{
      console.log(user);
      this.user=user;
      this.artist = this.navParams.get('artist');
      this.indexForfait = this.navParams.get('indexForfait');

      this.price = this.artist.artistesforfaits[this.indexForfait].prix;

      this.datanew.email = this.user.email;
      this.datanew.lname = this.user.surname;
      this.datanew.fname = this.user.name;
      this.datanew.amount = this.artist.artistesforfaits[this.indexForfait].prix;
      this.datanew.user_id = this.user.id;
      this.datanew.af_id = this.artist.artistesforfaits[this.indexForfait].id;
      // this.data.montant = this.price;
    })
  }

  submit(){
  }

  openWebpage(url:string){

    const options : InAppBrowserOptions = {
      hideurlbar: 'yes'
    }

    const browser = this.iab.create(url,'_self',options);
    browser.on('exit').subscribe(event => {
      // this.navCtrl.setRoot(HomePage);
      this.navCtrl.popAll();
        });

  }

  payer() {
    if(this.connectivityService.isOnline()){
      if (this.datanew.phone == '') {
        this.utils.makeToast('Veuillez entrer votre numéro de téléphone').present();
        return;
      } 
      if (this.datanew.channel == '') {
        this.utils.makeToast('Veuillez sélectionner votre moyen de paiement').present();
        return;
      } 
      this.openWebpage("http://webivoiremedia.net/paiement/index.php?phone="+this.datanew.phone+"&channel="+this.datanew.channel+"&amount="+this.datanew.amount+"&email="+this.datanew.email+"&fname="+this.datanew.fname+"&lname="+this.datanew.lname+"&af_id="+this.datanew.af_id+"&user_id="+this.datanew.user_id);
      // let loader = this.utils.makeLoader('');
      // loader.present().then(()=>{
      //   console.log(this.data)
      //   this.authPrvd.payer(this.data).then(response =>{
      //       console.log(response);
      //       this.authPrvd.getUrl(response.toString()).then(responseu =>{
      //         loader.dismiss();
      //         console.log('responseu',responseu);
      //         this.openWebpage(responseu.toString());
      //        }).catch((error)=>{
      //       console.log(error);
      //       loader.dismiss();
      //       this.utils.makeToast('erreur lors de la demande').present();
      //     })
      //       // this.navCtrl.popAll();
      //       // this.app.getRootNav().setRoot('SuccesPage',{text: 'votre demande sera effective après confirmation du paiement mobile money sur votre numéro'});
      //       // this.navCtrl.setRoot('SuccesPage',{text: 'votre demande sera effective après confirmation du paiement mobile money sur votre numéro'})
      //   }).catch((error)=>{
      //     console.log(error);
      //     loader.dismiss();
      //     this.utils.makeToast('erreur lors du payement').present();
      //   })
      // })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }
}
