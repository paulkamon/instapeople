import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ArtistProvider } from "../../providers/artist/artist";
import { VideoProvider } from "../../providers/video/video";
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { UtilsProvider } from '../../providers/utils/utils';
/**
 * Generated class for the ArtisteDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-artiste-detail',
  templateUrl: 'artiste-detail.html',
})
export class ArtisteDetailPage {
  hasSubscription:boolean;
  hasNoSubscription:boolean;

  user;
  artist = {
    id: '',    
    nom: '',
    photo: {
      id: '',    
      path: ''
    },
    artistesforfaits: [{
      id: '',    
      prix: '',
      forfait: {
        libelle: '',
        dureenjour:0    
      }
    }]
  };

  videoList:any[] = [{
    id: '',    
    titre: '',
    artiste_id:'',
    description:'',
    cover: {
      id: '',    
      path: ''
    },
    fichier: {
      id: '',    
      path: ''
    },
  }];

  photoUrl;
  nom;
  noVideo: boolean;

  constructor(public navParams: NavParams, public artistPrvd: ArtistProvider, public vidPrvd: VideoProvider,
     public navCtrl: NavController,
     public storage: Storage,public alertCtrl: AlertController,
    private connectivityService: ConnectivityProvider,private utils: UtilsProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArtisteDetailPage');
    this.artist = this.navParams.get('artist');
    this.nom = this.artist.nom;
    console.log('Artist '+this.artist);
    if(this.artist.photo){   
      this.photoUrl = this.artist.photo.path;
    }else{
      this.photoUrl = "assets/man.jpg";
    }
  }

  close() {
    this.navCtrl.pop();   
  }
 
  pushPay(){
    this.navCtrl.push('PaymentPage');
  }

  ionViewWillEnter(){
    this.storage.get('user').then((user)=>{
      console.log(user);
      this.user=user;
      if (!this.user) {
        this.navCtrl.pop();
      }else{
        this.getArtistVideo(this.artist.id, this.user.id)
      }
    })
  }

  forfaitSheet(){
    this.utils.makeActionSheet('Choisir un abonnement',[{
      icon: 'pricetag',
      text: 'forfait '+this.artist.artistesforfaits[0].forfait.libelle+' '+this.artist.artistesforfaits[0].prix+' F',
      handler: () => {
        // this.navCtrl.push('PaymentPage',{artist: this.artist, indexForfait: 0});
        if(this.hasNoSubscription) this.showConfirm(this.artist.artistesforfaits[0].forfait.dureenjour,0)
        else this.showConfirmRenew(this.artist.artistesforfaits[0].forfait.dureenjour,0);
      }
    },{
      icon: 'pricetag',
      text: 'forfait '+this.artist.artistesforfaits[1].forfait.libelle+' '+this.artist.artistesforfaits[1].prix+' F',
      handler: () => {
        // this.navCtrl.push('PaymentPage',{artist: this.artist, indexForfait: 1});
        if (this.hasNoSubscription) {
          this.showConfirm(this.artist.artistesforfaits[1].forfait.dureenjour,1);   
        } else this.showConfirmRenew(this.artist.artistesforfaits[1].forfait.dureenjour,1);  

        }
    }]).present();
  }
  // ngOnDestroy(){
  //   this.utils.makeToast('destroyed').present();
  // }

  showConfirm(days,indexf) {
    const confirm = this.alertCtrl.create({
      // title: 'Renouleve?',
      message: 'Votre abonnement sera actif pour une durée de '+days+' jours',
      buttons: [
        {
          text: 'Annuler',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Continuer',
          handler: () => {
            console.log('Agree clicked');
            this.navCtrl.push('PaymentPage',{artist: this.artist, indexForfait: indexf});
          }
        }
      ]
    });
    confirm.present();
  }
   
  showConfirmRenew(days,indexf) {
    const confirm = this.alertCtrl.create({
      // title: 'Renouleve?',
      message: 'Votre abonnement sera renouveler pour une durée de '+days+' jours',
      buttons: [
        {
          text: 'Annuler',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Continuer',
          handler: () => {
            console.log('Agree clicked');
            this.navCtrl.push('PaymentPage',{artist: this.artist, indexForfait: indexf});
          }
        }
      ]
    });
    confirm.present();
  }

  playVideo(i){
    this.vidPrvd.playVideo(this.videoList[i].fichier.path)
  }

  public imgVidPath(photo){
    if (photo == "" || photo == null || photo==undefined) {
      return "assets/smile.png";
    } else {
      return photo.path;
    }
    
  }
  getArtistVideo(artistId,clientId) {
    if(this.connectivityService.isOnline()){
        this.vidPrvd.getArtistVideo(artistId,clientId).then(response =>{
          console.log(response);
          this.hasSubscription = true;
          this.videoList = response.data.reverse();
          if (this.videoList == undefined) {
            this.noVideo=true;
          }
        }).catch((error)=>{
          
          this.hasNoSubscription = true;
          console.log(error);
            this.utils.makeToast(error.message).present();
        })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }

}
