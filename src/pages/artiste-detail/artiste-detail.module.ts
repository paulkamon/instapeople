import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArtisteDetailPage } from './artiste-detail';

@NgModule({
  declarations: [
    ArtisteDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ArtisteDetailPage),
  ],
})
export class ArtisteDetailPageModule {}
