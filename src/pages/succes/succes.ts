import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { HomePage } from '../home/home';
import { MyApp } from '../../app/app.component';
/**
 * Generated class for the SuccesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-succes',
  templateUrl: 'succes.html',
})
export class SuccesPage {
  text:string;

  constructor(public app: App, public navCtrl: NavController, public navParams: NavParams) {
    this.text = navParams.get('text');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SuccesPage');
  }

  returnToHome(){
    this.app.getRootNav().setRoot(HomePage);
    // window.location.reload();
    // this.navCtrl.setRoot(MyApp);
  }
}
