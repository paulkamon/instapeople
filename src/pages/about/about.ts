import { Component, ViewChild } from '@angular/core';
import { NavController,AlertController,Events  } from 'ionic-angular';
import { NavParams, Nav, App } from 'ionic-angular';
import {ViewController} from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { UtilsProvider } from '../../providers/utils/utils';

import { VideoProvider } from "../../providers/video/video";
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  pseudo:string;
  email:string;
  photoUrl:string;
  user;

   // Reference to the side menus root nav
   @ViewChild('content') nav: Nav;

  constructor(public events: Events,private connectivityService: ConnectivityProvider,
    public vidPrvd: VideoProvider,public navCtrl: NavController, public storage: Storage,
    private utils: UtilsProvider,
    private viewCtrl: ViewController,public modalCtrl: ModalController,public alertCtrl: AlertController) {
    events.subscribe('user:created', (user, time) => {
      // user and time are the same arguments passed in `events.publish(user, time)`
      console.log('Welcome', user.name, 'at', time);
      this.viewCtrl._willEnter();
    });
  }

  ionViewWillEnter(){
    this.storage.get('user').then((user)=>{
      console.log(user);
      this.user=user;
      if (user) {
        this.pseudo = user.name+" "+user.surname;
        this.email = user.email;
        if(user.lien_avatar){   
          this.photoUrl = user.lien_avatar;
        }else{
          this.photoUrl = 'assets/imgs/profile.png';
        }
        this.nav.setRoot(HomePage);
      } else {
        this.pseudo = "";
        this.email = "";
        this.nav.setRoot(HomePage);
      }
   }).catch(()=>{
    this.nav.setRoot(HomePage);
  });
   
  }

  home() {
    // this.viewCtrl._willEnter();
    this.nav.setRoot(HomePage);
    }
    
  login(){
    this.nav.setRoot('LoginPage',{});
  }

  register(){
    this.nav.setRoot('RegisterPage');
  }

  edit(){
    this.nav.setRoot('EditProfilPage',{});
  }

  picture(){
    this.nav.setRoot('PicturePage',{});
  }

  abonnements(){
    this.nav.setRoot('MesAbonnementsPage')
  }

  artistes(){
    this.nav.setRoot('ArtistesPage')
  }

  presentModal() {
    const modal = this.modalCtrl.create('PicturePage');
    modal.onDidDismiss(data => {
      console.log(data);
      this.viewCtrl._willEnter();
    });
    modal.present();
  }

  logout(){
    // this.navCtrl.setRoot(LoginPage,{});  
  this.storage.clear().then(()=>{
          // this.app.getRootNav().setRoot(LoginPage);
        console.log('cleared');
        window['plugins'].OneSignal.removeExternalUserId();
        this.viewCtrl._willEnter();
      })
      .catch(()=>{
        this.utils.makeToast('Veuillez réessayer svp!').present();
      });

  }

  showConfirm() {
    const confirm = this.alertCtrl.create({
      // title: 'Use this lightsaber?',
      message: 'voulez vous vraiment vous déconnecter?',
      buttons: [
        {
          text: 'annuler',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'confirmer',
          handler: () => {
            console.log('Agree clicked');
            this.logout();
          }
        }
      ]
    });
    confirm.present();
  }
}
