import { Component } from '@angular/core';
import { IonicPage, NavController, App, NavParams, AlertController, Events } from 'ionic-angular';
import { UtilsProvider } from '../../providers/utils/utils';
import { ConnectivityProvider } from '../../providers/connectivity/connectivity';
import { Storage } from '@ionic/storage';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { AuthProvider } from "../../providers/auth/auth";
import { HomePage } from '../../pages/home/home';

/**
 * Generated class for the EditProfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-profil',
  templateUrl: 'edit-profil.html',
})
export class EditProfilPage {
  picture:any;

  photoUrl:string;

  userId;
  data = {
    name:'',
    surname:'',
    email:'',
    password:'',
    detailsphoto:''
  };

  data2 = {
    name:'',
    surname:'',
    email:'',
    password:'',
    detailsphoto:''
  };
  // data = {
  //   fan_id :'',
  //   firstname: '',
  //   name: '',    
  //   pseudo: '',
  //   email: '',
  // };
  cpassword;

  data1:any={
    fan_id :''
  };
  imageURI:any;
imageFileName:any;
token;
  password: any;
  oldpassword:any;

  constructor(public events: Events, public app: App, public navCtrl: NavController, public navParams: NavParams, private utils: UtilsProvider,
    public storage: Storage,
    private connectivityService: ConnectivityProvider,
    public camera: Camera,public alertCtrl: AlertController,public authPrvd: AuthProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditProfilPage');
    this.storage.get('user').then((user)=>{
      console.log(user);
      this.data.name = user.name;
      this.data.surname = user.surname;
      this.data.email = user.email;
      this.userId = user.id;
      this.photoUrl = user.lien_avatar;
     
      this.data2.name = user.name;
      this.data2.surname = user.surname;
      this.data2.email = user.email;
      this.photoUrl = user.lien_avatar;

      this.storage.get('password').then((password)=>{
        console.log(password);
        this.password=password;
        })
      }) 
  }

  update() {
    if(this.connectivityService.isOnline()){
      
      // if (this.oldpassword && this.oldpassword != this.password) {
      //   this.utils.makeToast('mauvais mot de passe').present();
      //   return;
      // } 
      let loader = this.utils.makeLoader('Modification en cours...');
      loader.present().then(()=>{
        this.authPrvd.update(this.data , this.userId).then(response =>{
            loader.dismiss();
            console.log(response);
            this.storage.set('user', response.user).then((data) =>{
              // this.storage.set('password', this.data.password);
              this.utils.makeToast('Modification Réussie').present();
              // this.navCtrl.pop();
              this.app.getRootNav().setRoot(HomePage);
              this.events.publish('user:created', response.user, Date.now());

            })
        }).catch((error)=>{
          console.log(error);
  
          loader.dismiss();
          this.utils.makeToast('erreur lors du chargement').present();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }

  updatePwd() {
    if(this.connectivityService.isOnline()){
      if (!this.data2.password) {
        this.utils.makeToast('Entrez nouveau mot de passe').present();
        return;
      }
      if (this.oldpassword && this.oldpassword != this.password) {
        this.utils.makeToast('mauvais mot de passe actuel').present();
        return;
      } 
      if (this.data2.password != this.cpassword) {
        this.utils.makeToast('Votre mot de passe n\'est pas conforme à celui de la confirmation').present();
        return;
      }
      let loader = this.utils.makeLoader('Modification en cours...');
      loader.present().then(()=>{
        this.authPrvd.update(this.data2 , this.userId).then(response =>{
            loader.dismiss();
            console.log(response);
            this.storage.set('user', response.user).then((data) =>{
              this.storage.set('password', this.data2.password);
              this.utils.makeToast('Modification Réussie').present();
              // this.navCtrl.pop();
              this.app.getRootNav().setRoot(HomePage);
              this.events.publish('user:created', response.user, Date.now());

            })
        }).catch((error)=>{
          console.log(error);
  
          loader.dismiss();
          this.utils.makeToast('erreur lors du chargement').present();
        })
      })
    }
    else{
      this.utils.makeToast('Veuillez vous connecter à internet').present();
    }
  }


  actionSheet(){
    this.utils.makeActionSheet('Prendre une photo',[{
      icon: 'aperture',
      text: 'Galerie',
      handler: () => {
        this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
      }
    },{
      icon: 'camera',
      text: 'Camera',
      handler: () => {
        this.takePicture(this.camera.PictureSourceType.CAMERA);
      }
    }]).present();
  }

  takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options : CameraOptions = {
      quality: 50,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      this.picture = 'data:image/jpeg;base64,' + imagePath;
      this.data.detailsphoto = this.picture;
      this.photoUrl = this.picture;
    });
  }


}
