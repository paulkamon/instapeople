import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
// import { ConstantVariable } from '../../app/constant-variable';
// import { CameraOptions, Camera } from '@ionic-native/camera';
// import { UtilsProvider } from '../../providers/utils/utils';
// import { UserProvider } from "../../providers/user/user";
// import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

/**
 * Generated class for the PicturePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-picture',
  templateUrl: 'picture.html',
})
export class PicturePage {
  photoUrl:string;
  picture:any;

  data = {
    media:''
  };
  data1={
    fan_id :''
  };

  imageURI:any;
imageFileName:any;
token;
 
  constructor(public navCtrl: NavController, public navParams: NavParams,public storage: Storage, /* public camera: Camera,
    public userPrvd: UserProvider,
    private utilsPrvd: UtilsProvider,public viewCtrl: ViewController,public loadingCtrl: LoadingController,public toastCtrl: ToastController,
    private transfer: FileTransfer, */) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PicturePage');
    this.storage.get('user').then((user)=>{
      console.log(user);
      this.photoUrl = user.lien_avatar;
    })
  }

  // actionSheet(){
  //   this.utilsPrvd.makeActionSheet('Prendre une photo',[{
  //     icon: 'aperture',
  //     text: 'Galerie',
  //     handler: () => {
  //       this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
  //     }
  //   },{
  //     icon: 'camera',
  //     text: 'Camera',
  //     handler: () => {
  //       this.takePicture(this.camera.PictureSourceType.CAMERA);
  //     }
  //   }]).present();
  // }

  // takePicture(sourceType) {
  //   // Create options for the Camera Dialog
  //   var options : CameraOptions = {
  //     quality: 50,
  //     sourceType: sourceType,
  //     destinationType: this.camera.DestinationType.FILE_URI,
  //     encodingType: this.camera.EncodingType.JPEG,
  //     saveToPhotoAlbum: false,
  //     correctOrientation: true
  //   };

  //   // Get the data of an image
  //   this.camera.getPicture(options).then((imagePath) => {
  //     this.picture = 'data:image/jpeg;base64,' + imagePath;
  //     this.imageURI = imagePath;
  //     if(this.picture){   
  //       this.photoUrl = this.imageURI;
  //     }
  //     // this.data.media = this.picture;
  //   });
  // }

  cancel(){
    this.navCtrl.pop();
  }
  edit(){
    // this.storage.set('picture', this.photoUrl).then((data) =>{
    //   // this.navCtrl.pop();
    //   // this.viewCtrl.dismiss({ 'foo': 'bar' });
    //   this.uploadFile();
    // })
  }

  //   uploadFile() {
  //     let loader = this.loadingCtrl.create({
  //       content: "En cours..."
  //     });
  //     loader.present();
  //     const fileTransfer: FileTransferObject = this.transfer.create();
    
  //     let options: FileUploadOptions = {
  //       fileKey: 'media',
  //       fileName: 'media',
  //       chunkedMode: false,
  //       mimeType: "image/jpeg",
  //       params: this.data1,
  //       headers: {'Accept': "application/json", 'Authorization':'Bearer '+this.token}
  //     }
    
  //     fileTransfer.upload(this.imageURI, ConstantVariable.apiUrl+ConstantVariable.updatepp, options)
  //       .then((data) => {
  //       console.log(data+" Uploaded Successfully");
  //       // this.imageFileName = "http://192.168.0.7:8080/static/images/ionicfile.jpg"
  //       loader.dismiss();
  //       this.presentToast("Envoi réussi");
  //       this.viewCtrl.dismiss({ 'foo': 'bar' });
  //     }, (err) => {
  //       console.log(err);
  //       loader.dismiss();
  //       this.presentToast("veuillez reesayer");
  //     });
  //   }
  
  //   presentToast(msg) {
  //     let toast = this.toastCtrl.create({
  //       message: msg,
  //       duration: 3000,
  //       position: 'bottom'
  //     });
    
  //     toast.onDidDismiss(() => {
  //       console.log('Dismissed toast');
  //     });
    
  //     toast.present();
  //   }
  
}
